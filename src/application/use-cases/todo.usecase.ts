
import { Injectable, Logger } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { RecordModel } from "domain/models/record.model";
import { ToDoModel } from "domain/models/todo.model";
import { IBaseRes } from "infrastructure/interface/base.interface";
import { IObjectId, IRecords } from "infrastructure/interface/todo.interface";
import { ToDo, ToDoDocument } from "infrastructure/schemas/todo.schema";
import { ServiceResponse } from "infrastructure/utils/service.res";
import { Model } from "mongoose";

@Injectable()
export class ToDoUseCase {
  private readonly logger = new Logger(ToDoUseCase.name);
  public serviceRes = new ServiceResponse();
  constructor(@InjectModel(ToDo.name) private todoModel: Model<ToDoDocument>) {}

  async addRecord(
    owner_id: string,
    data: ToDoModel
  ): Promise<IObjectId | IBaseRes> {
    this.logger.log(`Добавляем запись`);
    const result = await this.todoModel.create({
      owner: owner_id,
      title: data.title,
      description: data.description,
    });
    if (result) {
      this.logger.log(`Запись успешно добавлена`);
      return this.serviceRes.uniqueSuccessRes({ id: result._id });
    } else {
      this.logger.log(`Ошибка при добавление записи`);
      return this.serviceRes.internalServerError();
    }
  }

  async getRecords(owner_id: string): Promise<IRecords | IBaseRes> {
    this.logger.log(`Получаем список записей`);
    const result = await this.todoModel
      .find({ owner: owner_id })
      .select(["title", "description"]);
    if (result) {
      this.logger.log(`Записи получены`);
      return this.serviceRes.uniqueSuccessRes(result);
    } else {
      this.logger.log(`Записи не найдены`);
      return this.serviceRes.recordDoesntExist();
    }
  }

  async deleteRecord(
    owner: string,
    data: RecordModel
  ): Promise<IObjectId | IBaseRes> {
    this.logger.log(`Проверяем наличие записи`);
    const result = await this.todoModel.findById({
      _id: data.id,
      owner: owner,
    });
    if (result) {
      this.logger.log(`Удаляем запись`);
      const delRecord = await this.todoModel.deleteOne({
        _id: data.id,
        owner: owner,
      });
      console.log(delRecord);
      return this.serviceRes.uniqueSuccessRes({ id: data.id });
    } else {
      this.logger.log(`Запись не существует`);
      return this.serviceRes.recordDoesntExist();
    }
  }
}
