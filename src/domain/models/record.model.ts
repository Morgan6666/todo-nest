import { IEntity } from "domain/shared/entity.interface";

export class RecordModel implements IEntity {
  id: string;

  constructor(id: string) {
    this.id = id;
  }
  equals(entity: IEntity): boolean {
    if (!(entity instanceof RecordModel)) return false;
    return this.id === entity.id;
  }
}
