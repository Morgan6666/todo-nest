import { IEntity } from "domain/shared/entity.interface";

export class ToDoModel implements IEntity {
    title: string;
    description: string;

    constructor(
        title: string,
        description: string
    ){
        this.title = title;
        this.description = description;
    }
    equals(entity: IEntity): boolean {
        if (!(entity instanceof ToDoModel)) return false;
        return this.title === entity.title;
      }


    
}

