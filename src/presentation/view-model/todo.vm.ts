import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { ToDoModel } from "domain/models/todo.model";

export class ToDoVM {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: "Title",
    example: "Tratatattata",
  })
  title: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: "Description",
    example: "Tibidoch",
  })
  description: string;

  static fromViewModel(vm: ToDoVM): ToDoModel {
    return new ToDoModel(vm.title, vm.description);
  }
}
