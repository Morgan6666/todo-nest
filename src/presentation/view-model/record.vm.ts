import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { RecordModel } from "domain/models/record.model";

export class RecordVM {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: "Object id",
    example: "6479519087e741b364ddb433",
  })
  id: string;
  static fromViewModel(vm: RecordVM): RecordModel {
    return new RecordModel(vm.id);
  }
}
