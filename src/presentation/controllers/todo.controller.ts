import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Req,
  UseGuards,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { ToDoUseCase } from "application/use-cases/todo.usecase";
import { ToDoModel } from "domain/models/todo.model";
import { RecordModel } from "domain/models/record.model";
import { AuthJWTGuard } from "infrastructure/guards/auth.guard";
import { RecordVM } from "presentation/view-model/record.vm";
import { ToDoVM } from "presentation/view-model/todo.vm";

@ApiTags("Todo")
@Controller("todo")
export class ToDoContoroller {
  constructor(private readonly todoUseCase: ToDoUseCase) {}

  @UseGuards(AuthJWTGuard)
  @Post("create")
  async getUser(@Body() data: ToDoVM, @Req() req) {
    const id: string = req.id;
    const result = await this.todoUseCase.addRecord(
      id,
      ToDoVM.fromViewModel(data)
    );
    return result;
  }

  @UseGuards(AuthJWTGuard)
  @Get("get")
  async getRecords(@Req() req) {
    const id: string = req.id;
    const result = await this.todoUseCase.getRecords(id);
    return result;
  }

  @UseGuards(AuthJWTGuard)
  @Delete("delete")
  async deleteRecord(@Req() req, @Body() data: RecordModel) {
    const id = req.id;
    const result = await this.todoUseCase.deleteRecord(
      id,
      RecordVM.fromViewModel(data)
    );
    return result;
  }
}
