import { IBaseRes } from "./base.interface";



export type IRecord = {
    title: string,
    description: string
}

export interface IObjectId extends IBaseRes {
    content: {
        id: string;
    }
}

export interface IRecords extends IBaseRes {
    content: {
        [index: number]: { IRecord }
    }
}