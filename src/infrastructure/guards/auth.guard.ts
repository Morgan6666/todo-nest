import { HttpService } from "@nestjs/axios";
import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  UnauthorizedException,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { IBaseRes } from "infrastructure/interface/base.interface";
import { ServiceResponse } from "infrastructure/utils/service.res";
import {
  AUTH_PORT,
  BASE_HOST,
  GET_USER_URL,
} from "infrastructure/utils/url.const";
import { result } from "lodash";
import { defaultArgs } from "puppeteer";

@Injectable()
export class AuthJWTGuard implements CanActivate {
  private readonly logger = new Logger(AuthJWTGuard.name);
  public serviceRes = new ServiceResponse();
  constructor(private readonly httpService: HttpService) {}

  async canActivate(context: ExecutionContext) {
    this.logger.log("Получаем токен");
    const req = context.switchToHttp().getRequest();
    const token: string | undefined = this.extractTokenFromHeader(req);
    if (!token) {
      throw  new HttpException('Bad token', HttpStatus.FORBIDDEN);
    }
    try {
      const result = await this.httpService.axiosRef.get(
        BASE_HOST.concat(AUTH_PORT, GET_USER_URL),
        { headers: this.httpHeaderGenerate(token) }
      );
      if (result.data.code != 200) {

        throw  new HttpException('Bad token', HttpStatus.FORBIDDEN);
      } else {
        req["id"] = result.data["content"].id;

      }
    } catch {
      throw  new HttpException('Bad token', HttpStatus.FORBIDDEN);
    }

    return true;
  }

  httpHeaderGenerate(token: string) {
    const headersReq = {
      "Content-Type": "application/json",
      Authorization: `${token}`,
    };
    return headersReq;
  }
  private extractTokenFromHeader(request: Request): string | undefined {
    
    const token: string = request.headers["authorization"];
    return token;
  }
}
