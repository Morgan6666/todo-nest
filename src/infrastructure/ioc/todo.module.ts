import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ToDoUseCase } from "application/use-cases/todo.usecase";
import { ToDo, ToDoSchema } from "infrastructure/schemas/todo.schema";
import { ToDoContoroller } from "presentation/controllers/todo.controller";



@Module({
  imports: [
    HttpModule,
    MongooseModule.forRoot("mongodb://localhost:27018"),
    MongooseModule.forFeature([{ name: ToDo.name, schema: ToDoSchema, }, ]),
  ],
  controllers: [ToDoContoroller],
  providers: [ToDoUseCase],
  
})
export class ToDoModule {}
