

import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";


export type ToDoDocument = ToDo & Document;

@Schema({timestamps: false, versionKey: false})
export class ToDo {   
    @Prop({required: true})
    owner: string;

    @Prop({required: true})
    title: string;

    @Prop({required: true})
    description: string;
}



export const ToDoSchema = SchemaFactory.createForClass(ToDo, );