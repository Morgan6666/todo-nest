import {
  CODE_200,
  CODE_409,
  RECORD_DOESNT_EXISST,
  BAD_ID_OR_PASSWORD,
  BAD_TOKEN,
  CODE_401,
  SUCCESS,
  INTERNAL_SERVER_ERROR,
} from "./message.const";
import { CODE_403, CODE_500, CODE_404 } from "./message.const";
import { SUCCESS_FALSE, SUCCESS_TRUE } from "./message.const";

export class ServiceResponse {
  uniqueServiceErrorRes(
    success: boolean,
    code: number,
    message: string,
    content: any
  ) {
    return {
      success: success,
      message: message,
      code: code,
      content: content,
    };
  }

  badIdOrPassword() {
    return this.uniqueServiceErrorRes(
      SUCCESS_TRUE,
      CODE_401,
      BAD_ID_OR_PASSWORD,
      {}
    );
  }
  badToken() {
    return this.uniqueServiceErrorRes(SUCCESS_TRUE, CODE_403, BAD_TOKEN, {});
  }

  uniqueServiceRes(
    success: boolean,
    code: number,
    message: string,
    content: object
  ) {
    return {
      success: success,
      message: message,
      code: code,
      content: content,
    };
  }

  uniqueSuccessRes(content: Object) {
    return this.uniqueServiceRes(SUCCESS_TRUE, CODE_200, SUCCESS, content);
  }

  recordDoesntExist() {
    return this.uniqueServiceErrorRes(
      SUCCESS_FALSE,
      CODE_404,
      RECORD_DOESNT_EXISST,
      {}
    );
  }
  internalServerError() {
    return this.uniqueServiceErrorRes(
      SUCCESS_FALSE,
      CODE_500,
      INTERNAL_SERVER_ERROR,
      {}
    );
  }
}
