
  ```bash
  Простой запуск: npm i (ci) -> npm run start 
  Чтобы запустить бэк и mongo через docker, используйте комманду docker-compose up --force-recreate -d
  Бэк доступен на 3012 порту, mongo на 27018
  Запросы:
    1. /todo/create
    URL: http://localhost:3012/todo/create
    Method: POST
    Auth: Barear
    Payload: 
      {
        "title": "teststts",
        "description": "blbllblblbl"
      }
    Res: {
        "success": true,
        "message": "Успешно",
        "code": 200,
        "content": {
          "id": "6479645d683ee955caf16285"
        }
}

    2. /todo/get
    URL: http://localhost:3012/todo/get
    Method: Get
    Auth: Barear
    Res: 
        {
    "success": true,
    "message": "Успешно",
    "code": 200,
    "content": [
        {
            "_id": "64794f6960457da55091f5eb",
            "title": "teststts",
            "description": "blbllblblbl"
        },
        
          ]

    3. todo/delete
    URL: http://localhost:3012/todo/delete
    Method: DELETE
    Auth: Barear
    

## Поддержка

- mail - dbairamkulow
- tg - @DenisDeonis
