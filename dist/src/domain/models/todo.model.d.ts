import { IEntity } from "domain/shared/entity.interface";
export declare class ToDoModel implements IEntity {
    title: string;
    description: string;
    constructor(title: string, description: string);
    equals(entity: IEntity): boolean;
}
