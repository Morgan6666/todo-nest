"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToDoModel = void 0;
class ToDoModel {
    constructor(title, description) {
        this.title = title;
        this.description = description;
    }
    equals(entity) {
        if (!(entity instanceof ToDoModel))
            return false;
        return this.title === entity.title;
    }
}
exports.ToDoModel = ToDoModel;
//# sourceMappingURL=todo.model.js.map