import { IEntity } from "domain/shared/entity.interface";
export declare class RecordModel implements IEntity {
    id: string;
    constructor(id: string);
    equals(entity: IEntity): boolean;
}
