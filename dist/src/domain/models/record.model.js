"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecordModel = void 0;
class RecordModel {
    constructor(id) {
        this.id = id;
    }
    equals(entity) {
        if (!(entity instanceof RecordModel))
            return false;
        return this.id === entity.id;
    }
}
exports.RecordModel = RecordModel;
//# sourceMappingURL=record.model.js.map