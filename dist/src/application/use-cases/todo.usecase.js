"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var ToDoUseCase_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToDoUseCase = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const todo_schema_1 = require("../../infrastructure/schemas/todo.schema");
const service_res_1 = require("../../infrastructure/utils/service.res");
const mongoose_2 = require("mongoose");
let ToDoUseCase = ToDoUseCase_1 = class ToDoUseCase {
    constructor(todoModel) {
        this.todoModel = todoModel;
        this.logger = new common_1.Logger(ToDoUseCase_1.name);
        this.serviceRes = new service_res_1.ServiceResponse();
    }
    async addRecord(owner_id, data) {
        this.logger.log(`Добавляем запись`);
        const result = await this.todoModel.create({
            owner: owner_id,
            title: data.title,
            description: data.description,
        });
        if (result) {
            this.logger.log(`Запись успешно добавлена`);
            return this.serviceRes.uniqueSuccessRes({ id: result._id });
        }
        else {
            this.logger.log(`Ошибка при добавление записи`);
            return this.serviceRes.internalServerError();
        }
    }
    async getRecords(owner_id) {
        this.logger.log(`Получаем список записей`);
        const result = await this.todoModel
            .find({ owner: owner_id })
            .select(["title", "description"]);
        if (result) {
            this.logger.log(`Записи получены`);
            return this.serviceRes.uniqueSuccessRes(result);
        }
        else {
            this.logger.log(`Записи не найдены`);
            return this.serviceRes.recordDoesntExist();
        }
    }
    async deleteRecord(owner, data) {
        this.logger.log(`Проверяем наличие записи`);
        const result = await this.todoModel.findById({
            _id: data.id,
            owner: owner,
        });
        if (result) {
            this.logger.log(`Удаляем запись`);
            const delRecord = await this.todoModel.deleteOne({
                _id: data.id,
                owner: owner,
            });
            console.log(delRecord);
            return this.serviceRes.uniqueSuccessRes({ id: data.id });
        }
        else {
            this.logger.log(`Запись не существует`);
            return this.serviceRes.recordDoesntExist();
        }
    }
};
ToDoUseCase = ToDoUseCase_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(todo_schema_1.ToDo.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], ToDoUseCase);
exports.ToDoUseCase = ToDoUseCase;
//# sourceMappingURL=todo.usecase.js.map