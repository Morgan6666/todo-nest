import { RecordModel } from "domain/models/record.model";
import { ToDoModel } from "domain/models/todo.model";
import { IBaseRes } from "infrastructure/interface/base.interface";
import { IObjectId, IRecords } from "infrastructure/interface/todo.interface";
import { ToDoDocument } from "infrastructure/schemas/todo.schema";
import { ServiceResponse } from "infrastructure/utils/service.res";
import { Model } from "mongoose";
export declare class ToDoUseCase {
    private todoModel;
    private readonly logger;
    serviceRes: ServiceResponse;
    constructor(todoModel: Model<ToDoDocument>);
    addRecord(owner_id: string, data: ToDoModel): Promise<IObjectId | IBaseRes>;
    getRecords(owner_id: string): Promise<IRecords | IBaseRes>;
    deleteRecord(owner: string, data: RecordModel): Promise<IObjectId | IBaseRes>;
}
