export interface IBaseRes {
    success: boolean;
    code: number;
    message: string;
    content: any;
}
