"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceResponse = void 0;
const message_const_1 = require("./message.const");
const message_const_2 = require("./message.const");
const message_const_3 = require("./message.const");
class ServiceResponse {
    uniqueServiceErrorRes(success, code, message, content) {
        return {
            success: success,
            message: message,
            code: code,
            content: content,
        };
    }
    badIdOrPassword() {
        return this.uniqueServiceErrorRes(message_const_3.SUCCESS_TRUE, message_const_1.CODE_401, message_const_1.BAD_ID_OR_PASSWORD, {});
    }
    badToken() {
        return this.uniqueServiceErrorRes(message_const_3.SUCCESS_TRUE, message_const_2.CODE_403, message_const_1.BAD_TOKEN, {});
    }
    uniqueServiceRes(success, code, message, content) {
        return {
            success: success,
            message: message,
            code: code,
            content: content,
        };
    }
    uniqueSuccessRes(content) {
        return this.uniqueServiceRes(message_const_3.SUCCESS_TRUE, message_const_1.CODE_200, message_const_1.SUCCESS, content);
    }
    recordDoesntExist() {
        return this.uniqueServiceErrorRes(message_const_3.SUCCESS_FALSE, message_const_2.CODE_404, message_const_1.RECORD_DOESNT_EXISST, {});
    }
    internalServerError() {
        return this.uniqueServiceErrorRes(message_const_3.SUCCESS_FALSE, message_const_2.CODE_500, message_const_1.INTERNAL_SERVER_ERROR, {});
    }
}
exports.ServiceResponse = ServiceResponse;
//# sourceMappingURL=service.res.js.map