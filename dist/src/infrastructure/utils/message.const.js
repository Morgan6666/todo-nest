"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EMPTY_CONTENT = exports.INTERNAL_SERVER_ERROR = exports.SUCCESS_TRUE = exports.SUCCESS_FALSE = exports.SUCCESS = exports.CODE_401 = exports.CODE_409 = exports.CODE_200 = exports.CODE_403 = exports.CODE_500 = exports.CODE_404 = exports.BAD_TOKEN = exports.BAD_ID_OR_PASSWORD = exports.RECORD_DOESNT_EXISST = void 0;
exports.RECORD_DOESNT_EXISST = "Запись не найдена";
exports.BAD_ID_OR_PASSWORD = "Bad id or password";
exports.BAD_TOKEN = "Bad token";
exports.CODE_404 = 404;
exports.CODE_500 = 500;
exports.CODE_403 = 403;
exports.CODE_200 = 200;
exports.CODE_409 = 409;
exports.CODE_401 = 401;
exports.SUCCESS = "Успешно";
exports.SUCCESS_FALSE = false;
exports.SUCCESS_TRUE = true;
exports.INTERNAL_SERVER_ERROR = "Internal server error";
exports.EMPTY_CONTENT = {};
//# sourceMappingURL=message.const.js.map