export declare const RECORD_DOESNT_EXISST: string;
export declare const BAD_ID_OR_PASSWORD: string;
export declare const BAD_TOKEN: string;
export declare const CODE_404: number;
export declare const CODE_500: number;
export declare const CODE_403: number;
export declare const CODE_200: number;
export declare const CODE_409: number;
export declare const CODE_401: number;
export declare const SUCCESS: string;
export declare const SUCCESS_FALSE: boolean;
export declare const SUCCESS_TRUE: boolean;
export declare const INTERNAL_SERVER_ERROR: string;
export declare const EMPTY_CONTENT: Object;
