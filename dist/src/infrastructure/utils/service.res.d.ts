export declare class ServiceResponse {
    uniqueServiceErrorRes(success: boolean, code: number, message: string, content: any): {
        success: boolean;
        message: string;
        code: number;
        content: any;
    };
    badIdOrPassword(): {
        success: boolean;
        message: string;
        code: number;
        content: any;
    };
    badToken(): {
        success: boolean;
        message: string;
        code: number;
        content: any;
    };
    uniqueServiceRes(success: boolean, code: number, message: string, content: object): {
        success: boolean;
        message: string;
        code: number;
        content: object;
    };
    uniqueSuccessRes(content: Object): {
        success: boolean;
        message: string;
        code: number;
        content: object;
    };
    recordDoesntExist(): {
        success: boolean;
        message: string;
        code: number;
        content: any;
    };
    internalServerError(): {
        success: boolean;
        message: string;
        code: number;
        content: any;
    };
}
