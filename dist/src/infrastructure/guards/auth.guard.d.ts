import { HttpService } from "@nestjs/axios";
import { CanActivate, ExecutionContext } from "@nestjs/common";
import { ServiceResponse } from "infrastructure/utils/service.res";
export declare class AuthJWTGuard implements CanActivate {
    private readonly httpService;
    private readonly logger;
    serviceRes: ServiceResponse;
    constructor(httpService: HttpService);
    canActivate(context: ExecutionContext): Promise<boolean>;
    httpHeaderGenerate(token: string): {
        "Content-Type": string;
        Authorization: string;
    };
    private extractTokenFromHeader;
}
