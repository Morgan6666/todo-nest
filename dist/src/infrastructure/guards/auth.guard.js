"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AuthJWTGuard_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthJWTGuard = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const service_res_1 = require("../utils/service.res");
const url_const_1 = require("../utils/url.const");
let AuthJWTGuard = AuthJWTGuard_1 = class AuthJWTGuard {
    constructor(httpService) {
        this.httpService = httpService;
        this.logger = new common_1.Logger(AuthJWTGuard_1.name);
        this.serviceRes = new service_res_1.ServiceResponse();
    }
    async canActivate(context) {
        this.logger.log("Получаем токен");
        const req = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(req);
        if (!token) {
            throw new common_1.HttpException('Bad token', common_1.HttpStatus.FORBIDDEN);
        }
        try {
            const result = await this.httpService.axiosRef.get(url_const_1.BASE_HOST.concat(url_const_1.AUTH_PORT, url_const_1.GET_USER_URL), { headers: this.httpHeaderGenerate(token) });
            if (result.data.code != 200) {
                throw new common_1.HttpException('Bad token', common_1.HttpStatus.FORBIDDEN);
            }
            else {
                req["id"] = result.data["content"].id;
            }
        }
        catch (_a) {
            throw new common_1.HttpException('Bad token', common_1.HttpStatus.FORBIDDEN);
        }
        return true;
    }
    httpHeaderGenerate(token) {
        const headersReq = {
            "Content-Type": "application/json",
            Authorization: `${token}`,
        };
        return headersReq;
    }
    extractTokenFromHeader(request) {
        const token = request.headers["authorization"];
        return token;
    }
};
AuthJWTGuard = AuthJWTGuard_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [axios_1.HttpService])
], AuthJWTGuard);
exports.AuthJWTGuard = AuthJWTGuard;
//# sourceMappingURL=auth.guard.js.map