"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToDoModule = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const todo_usecase_1 = require("../../application/use-cases/todo.usecase");
const todo_schema_1 = require("../schemas/todo.schema");
const todo_controller_1 = require("../../presentation/controllers/todo.controller");
let ToDoModule = class ToDoModule {
};
ToDoModule = __decorate([
    (0, common_1.Module)({
        imports: [
            axios_1.HttpModule,
            mongoose_1.MongooseModule.forRoot("mongodb://localhost:27018"),
            mongoose_1.MongooseModule.forFeature([{ name: todo_schema_1.ToDo.name, schema: todo_schema_1.ToDoSchema, },]),
        ],
        controllers: [todo_controller_1.ToDoContoroller],
        providers: [todo_usecase_1.ToDoUseCase],
    })
], ToDoModule);
exports.ToDoModule = ToDoModule;
//# sourceMappingURL=todo.module.js.map