import { ToDoModel } from "domain/models/todo.model";
export declare class ToDoVM {
    title: string;
    description: string;
    static fromViewModel(vm: ToDoVM): ToDoModel;
}
