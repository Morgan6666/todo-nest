import { RecordModel } from "domain/models/record.model";
export declare class RecordVM {
    id: string;
    static fromViewModel(vm: RecordVM): RecordModel;
}
