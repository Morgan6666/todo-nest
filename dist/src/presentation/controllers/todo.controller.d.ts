import { ToDoUseCase } from "application/use-cases/todo.usecase";
import { RecordModel } from "domain/models/record.model";
import { ToDoVM } from "presentation/view-model/todo.vm";
export declare class ToDoContoroller {
    private readonly todoUseCase;
    constructor(todoUseCase: ToDoUseCase);
    getUser(data: ToDoVM, req: any): Promise<import("../../infrastructure/interface/base.interface").IBaseRes | import("../../infrastructure/interface/todo.interface").IObjectId>;
    getRecords(req: any): Promise<import("../../infrastructure/interface/base.interface").IBaseRes | import("../../infrastructure/interface/todo.interface").IRecords>;
    deleteRecord(req: any, data: RecordModel): Promise<import("../../infrastructure/interface/base.interface").IBaseRes | import("../../infrastructure/interface/todo.interface").IObjectId>;
}
