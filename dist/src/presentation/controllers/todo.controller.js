"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToDoContoroller = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const todo_usecase_1 = require("../../application/use-cases/todo.usecase");
const record_model_1 = require("../../domain/models/record.model");
const auth_guard_1 = require("../../infrastructure/guards/auth.guard");
const record_vm_1 = require("../view-model/record.vm");
const todo_vm_1 = require("../view-model/todo.vm");
let ToDoContoroller = class ToDoContoroller {
    constructor(todoUseCase) {
        this.todoUseCase = todoUseCase;
    }
    async getUser(data, req) {
        const id = req.id;
        const result = await this.todoUseCase.addRecord(id, todo_vm_1.ToDoVM.fromViewModel(data));
        return result;
    }
    async getRecords(req) {
        const id = req.id;
        const result = await this.todoUseCase.getRecords(id);
        return result;
    }
    async deleteRecord(req, data) {
        const id = req.id;
        const result = await this.todoUseCase.deleteRecord(id, record_vm_1.RecordVM.fromViewModel(data));
        return result;
    }
};
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.AuthJWTGuard),
    (0, common_1.Post)("create"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [todo_vm_1.ToDoVM, Object]),
    __metadata("design:returntype", Promise)
], ToDoContoroller.prototype, "getUser", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.AuthJWTGuard),
    (0, common_1.Get)("get"),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ToDoContoroller.prototype, "getRecords", null);
__decorate([
    (0, common_1.UseGuards)(auth_guard_1.AuthJWTGuard),
    (0, common_1.Delete)("delete"),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, record_model_1.RecordModel]),
    __metadata("design:returntype", Promise)
], ToDoContoroller.prototype, "deleteRecord", null);
ToDoContoroller = __decorate([
    (0, swagger_1.ApiTags)("Todo"),
    (0, common_1.Controller)("todo"),
    __metadata("design:paramtypes", [todo_usecase_1.ToDoUseCase])
], ToDoContoroller);
exports.ToDoContoroller = ToDoContoroller;
//# sourceMappingURL=todo.controller.js.map